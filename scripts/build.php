<?php
require_once('cli_init.php');

if (empty($argv[1]) || !is_dir($argv[1]) || !is_readable($argv[1])) {
    echo "Pass a readable app directory as the first parameter\n";
    print_usage();
    exit();
}
if (empty($argv[2]) || !is_dir($argv[2]) || !is_readable($argv[2])) {
    echo "Pass a writable output directory as the second parameter\n";
    print_usage();
    exit();
}
$src_dir = rtrim($argv[1], '/\\');
$target_dir = rtrim($argv[2], '/\\');
$build_config = isset($argv[3]) ? $argv[3] : 'default';

$version = is_file('VERSION') && is_readable('VERSION') ? file_get_contents('VERSION') : 'NO_VERSION_FILE';
$version = str_replace('.', '-', $version);


// 1. copy working app dir to temp directory
$temp_dir = create_temp_dir($target_dir, $build_config . '_' . (new DateTime())->format('Ymd-His') . '_');
echo "copy src dir into target: $src_dir -> $temp_dir\n";
copy_dir($src_dir, $temp_dir);

// 2. clean the copied app dir
echo "clean copied src dir\n";
$build_dir = $temp_dir . '/' . basename($src_dir);
$working_dir = getcwd();
chdir($build_dir);
include('clean.php');
chdir($working_dir);

// 3. zip the whole thing up
chdir($temp_dir);
echo "create zip\n";
$zip_name = 'archive.zip';
zip_dir(basename($src_dir), $zip_name);
$zip_file_contents = file_get_contents($zip_name);
$zip_md5_hex = hash('md5', $zip_file_contents);
echo "archive md5 sum: $zip_md5_hex\n";
$final_name = $build_config . '_' . $version . '_' . $zip_md5_hex . '.zip';
rename($zip_name, $final_name);
echo "created archive: $final_name\n";
echo "build complete\n";

/**************/

function print_usage() {
    global $argv;
    echo "USAGE:\n";
    echo "{$argv[0]} APP_DIR OUTPUT_DIR [BUILD_CONFIG]\n";
}