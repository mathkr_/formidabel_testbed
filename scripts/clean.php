<?php
require_once('cli_init.php');

echo 'clean dir: ' . getcwd() . "\n";

$clean_config_path = __DIR__ . '/clean_config.json';
$clean_config = file_get_contents($clean_config_path);
$clean_config = json_decode($clean_config, TRUE);
if (!(bool)$clean_config) {
    throw new Exception('Could not load and decode clean config file: ' . $clean_config_path);
}

$clean_files = $clean_config['files'];
$clean_dirs = [];
foreach ($clean_config['globs'] as $pattern) {
    $glob_list = glob($pattern, GLOB_NOSORT);
    foreach ($glob_list as $entry) {
        if (is_dir($entry)) {
            walk_dir(
                $entry,
                function ($path, $is_dir, $is_file, $is_link) use (&$clean_files, &$clean_dirs) {
                    if ($is_dir) {
                        $clean_dirs[] = $path;
                    } else if ($is_file || $is_link) {
                        $clean_files[] = $path;
                    }
                },
                TRUE
            );
        } else if (is_file($entry) || is_link($entry)) {
            $clean_files[] = $entry;
        }
    }
}

foreach ($clean_files as $file) {
    if (is_file($file) && unlink($file)) {
        echo "    rmfile: $file\n";
    } else if (file_exists($file)) {
        throw new Exception('Could not remove file: ' . $file);
    }
}
foreach ($clean_dirs as $dir) {
    if (rmdir($dir)) {
        echo "    rmdir:  $dir\n";
    } else if (file_exists($dir)) {
        throw new Exception('Could not remove dir: ' . $dir);
    }
}