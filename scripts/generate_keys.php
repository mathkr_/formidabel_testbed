<?php

// Just in case, since this script is for local testing only
if (PHP_SAPI != 'cli') {
    echo 'This script is CLI only: PHP_SAPI = ' . PHP_SAPI;
    exit();
}

require_once('formidabel/src/functions.php');

// This file generates users, passwords and keys for use in the testbed environment

echo "Generating timestamp secret key and writing to file..\n";
$timestamp_key = sodium_crypto_secretbox_keygen();
file_put_contents('formidabel/' . Config\TIMESTAMP_SECRET_KEY_FILE, $timestamp_key);

$fixed_seed = '';
$keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
for ($i = 0; $i < SODIUM_CRYPTO_BOX_SEEDBYTES; $i++) {
    $fixed_seed .= $keyspace[$i % strlen($keyspace)];
}
echo "Generating store keypair with fixed seed ($fixed_seed)..\n";
$store_keypair = sodium_crypto_box_seed_keypair($fixed_seed);
$store_public_key = sodium_crypto_box_publickey($store_keypair);
$store_secret_key = sodium_crypto_box_secretkey($store_keypair);

echo "Writing store public key to file..\n";
$store_key_file = fopen('formidabel/' . Config\STORE_PUBLIC_KEY_FILE, 'wb');
fwrite($store_key_file, $store_public_key);
fclose($store_key_file);

$users = [
    'admin'         => 'admin',
    'IchBinDerUser' => 'IchBinDasPasswort',
    'hunter1337'    => 'LebtDerHolzmichelNoch?',
];

echo "Generating users and encrypting store key per user with derived key..\n";
foreach ($users as $user => $pass) {
    $derived_key_salt = random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
    $derived_key_len = SODIUM_CRYPTO_SECRETBOX_KEYBYTES;
    $derived_key = sodium_crypto_pwhash(
        $derived_key_len,
        $pass,
        $derived_key_salt,
        Config\PWHASH_OPSLIMIT,
        Config\PWHASH_MEMLIMIT
    );

    $encrypted_store_key_nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
    $encrypted_store_key = sodium_crypto_secretbox(
        $store_secret_key,
        $encrypted_store_key_nonce,
        $derived_key
    );

    $users[$user] = [
        'pwhash'                    => sodium_crypto_pwhash_str($pass, Config\PWHASH_OPSLIMIT, Config\PWHASH_MEMLIMIT),
        'derived_key_salt'          => sodium_bin2base64($derived_key_salt, SODIUM_BASE64_VARIANT_ORIGINAL),
        'encrypted_store_key'       => sodium_bin2base64($encrypted_store_key, SODIUM_BASE64_VARIANT_ORIGINAL),
        'encrypted_store_key_nonce' => sodium_bin2base64($encrypted_store_key_nonce, SODIUM_BASE64_VARIANT_ORIGINAL),
    ];
}

echo "Writing users file..\n";
file_put_contents('formidabel/' . Config\USERS_FILE, json_encode($users, Config\JSON_ENCODE_FLAGS));