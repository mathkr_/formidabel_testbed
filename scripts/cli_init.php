<?php
if (PHP_SAPI != 'cli') {
    echo 'This script is CLI only: PHP_SAPI = ' . PHP_SAPI;
    exit();
}
set_error_handler(function ($sev, $str, $file, $line) {
    if (!(error_reporting() & $sev)) {
        return;
    }
    $msg = "Error($sev): $str in $file on line $line";
    if ($sev == E_NOTICE || $sev == E_WARNING) {
        throw new ErrorException($msg, 0, $sev, $file, $line);
    } else {
        echo $msg . "\n";
    }
});

/*********/

/**
 * @param callable $f function(string $path, bool $is_dir, bool $is_file, bool $is_link)
 */
function walk_dir(string $dir, callable $f, bool $depth_first = TRUE) {
    $dir = rtrim($dir, '/\\');
    if (!is_dir($dir) || !is_readable($dir)) {
        throw new Exception('Cannot read from directory: ' . $dir);
    }

    if (!$depth_first) {
        $f($dir, TRUE, FALSE, FALSE);
    }
    if ($handle = opendir($dir)) {
        while (($entry = readdir($handle)) !== FALSE) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            $path = $dir . '/' . $entry;
            if (is_dir($path)) {
                walk_dir($path, $f);
            } else if (is_file($path)) {
                $f($path, FALSE, TRUE, FALSE);
            } else if (is_link($path)) {
                $f($path, FALSE, FALSE, TRUE);
            } else {
                throw new Exception('Unexpected directory entry in walk_dir: ' . $path);
            }
        }
        closedir($handle);
        if ($depth_first) {
            $f($dir, TRUE, FALSE, FALSE);
        }
    } else {
        throw new Exception('Cannot open directory: ' . $dir);
    }
}

function copy_dir(string $dir, string $into) {
    $dir = rtrim($dir, '/\\');
    $into = rtrim($into, '/\\');

    if (!is_dir($dir) || !is_readable($dir)) {
        throw new Exception('Cannot read from directory: ' . $dir);
    }
    $basename = basename($dir);
    $target = $into . '/' . $basename;
    if (!is_dir($into) || !is_writable($into) || mkdir($target) === FALSE) {
        throw new Exception('Cannot write into directory: ' . $target);
    }

    if ($handle = opendir($dir)) {
        while (($entry = readdir($handle)) !== FALSE) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            $path = $dir . '/' . $entry;
            if (is_dir($path)) {
                copy_dir($path, $target);
            } else if (is_file($path)) {
                $dest = $target . '/' . $entry;
                if (copy($path, $dest) === FALSE) {
                    throw new Exception('Could not copy file: ' . $path . ' -> ' . $dest);
                }
            }
        }
        closedir($handle);
    } else {
        throw new Exception('Cannot open directory: ' . $dir);
    }
}

function create_temp_dir(string $base = NULL, string $prefix = 'tmp_'): string {
    $base = is_null($base) ? sys_get_temp_dir() : $base;
    $base = rtrim($base, '/\\');

    if (!is_dir($base) || !is_writable($base)) {
        throw new Exception('Cannot write to base dir: ' . $base);
    }
    $attempts = 10;
    while ($attempts > 0) {
        $random = bin2hex(random_bytes(4));
        $dir = $base . '/' . $prefix . $random;
        if (mkdir($dir) === TRUE) {
            return $dir;
        }
        --$attempts;
    }
    throw new Exception('Could not create temp dir in: ' . $base);
}

function zip_dir(string $dir, string $dest_file_path) {
    if (!is_dir($dir) || !is_readable($dir)) {
        throw new Exception('Could not read directory: ' . $dir);
    }
    if (file_exists($dest_file_path)) {
        throw new Exception('Zip target file already exists: ' . $dest_file_path);
    }

    $zip = new ZipArchive();
    if ($zip->open($dest_file_path, ZipArchive::CREATE) !== TRUE) {
        throw new Exception('Could not create zip archive: ' . $zip->getStatusString());
    }
    walk_dir(
        $dir,
        function ($path, $is_dir, $is_file, $is_link) use (&$zip) {
            $path = str_replace('\\', '/', $path);
            if ($is_dir) {
                if (!$zip->addEmptyDir($path)) {
                    throw new Exception("Could not add dir to zip archive ($path): " . $zip->getStatusString());
                }
            } else if ($is_file) {
                if (!$zip->addFile($path)) {
                    throw new Exception('Could not add file to zip archive: ' . $zip->getStatusString());
                }
            } else {
                throw new Exception("Unexpected entry in zip_dir (link:$is_link): " . $path);
            }
        },
        FALSE
    );
    if (!$zip->close()) {
        throw new Exception('Could not close zip file: ' . $zip->getStatusString());
    }
}