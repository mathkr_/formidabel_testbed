<?php
require_once('src/init.php');
require_once('src/forms.php');

if (match_request('POST', [])) {
    // Check if any fields are set. If so it's either a spam bot or somebody without JS.
    if (count($_POST) > 0) {
        // TODO: log
        // TODO: localization
        echo 'Please enable Javascript and reload the previous page to submit this form. Thank you!';
        http_response_code(400);
        exit();
    }

    $timestamp_secret_key = file_get_contents(Config\TIMESTAMP_SECRET_KEY_FILE);
    if ($timestamp_secret_key === FALSE) {
        error_log('Could not read timestamp secret key from file');
        http_response_code(500);
        exit();
    }

    $timestamp_nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
    $encrypted_timestamp = sodium_crypto_secretbox(
        $_SERVER['REQUEST_TIME'],
        $timestamp_nonce,
        $timestamp_secret_key
    );
    $submit_token =
        sodium_bin2base64($encrypted_timestamp, SODIUM_BASE64_VARIANT_ORIGINAL) . ':' .
        sodium_bin2base64($timestamp_nonce, SODIUM_BASE64_VARIANT_ORIGINAL);
    $response = [
        'submit_token'      => $submit_token,
        'request_period'    => Config\TIMESTAMP_REQUEST_PERIOD
    ];

    $json = json_encode($response, Config\JSON_ENCODE_FLAGS);
    header('Content-Type: application/json;charset=UTF-8');
    echo $json;
} else {
    http_response_code(400);
}