<?php
require_once('src/init.php');

session_start();
$_SESSION = array();
session_destroy();
header('Location: ' . Config\LOGIN_PATH);