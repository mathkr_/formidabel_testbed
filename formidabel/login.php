<?php
require_once('src/init.php');
require_once('src/template.php');

session_start();
if (isset($_SESSION[Config\SESSION_USER]) && isset($_SESSION[Config\SESSION_STORE_KEY])) {
    header('Location: index.php');
    exit();
}

$unsuccessful_login_attempt = FALSE;
if (match_request('POST', ['user' => NULL, 'pass' => NULL])) {
    $users_table = json_decode(file_get_contents(Config\USERS_FILE), TRUE);
    if (isset($users_table[$_REQUEST['user']])) {
        // TODO: make timing attacks to find existing users more difficult by always hashing the password, even if
        // the username doesn't exist

        $user = $users_table[$_REQUEST['user']];
        if (sodium_crypto_pwhash_str_verify($user['pwhash'], $_REQUEST['pass'])) {
            if (sodium_crypto_pwhash_str_needs_rehash($user['pwhash'], Config\PWHASH_OPSLIMIT, Config\PWHASH_MEMLIMIT)) {
                $users_table[$_REQUEST['user']]['pwhash'] = sodium_crypto_pwhash_str($_REQUEST['pass'], Config\PWHASH_OPSLIMIT, Config\PWHASH_MEMLIMIT);
                $users_json = json_encode($users_table, Config\JSON_ENCODE_FLAGS);
                if ($users_json) {
                    file_put_contents(Config\USERS_FILE, $users_json);
                }
            }

            $derived_key_salt = sodium_base642bin($user['derived_key_salt'], SODIUM_BASE64_VARIANT_ORIGINAL);
            $derived_key_len = SODIUM_CRYPTO_SECRETBOX_KEYBYTES;
            $derived_key = sodium_crypto_pwhash(
                $derived_key_len,
                $_REQUEST['pass'],
                $derived_key_salt,
                Config\PWHASH_OPSLIMIT,
                Config\PWHASH_MEMLIMIT
            );
            $encrypted_store_key = sodium_base642bin($user['encrypted_store_key'], SODIUM_BASE64_VARIANT_ORIGINAL);
            $encrypted_store_key_nonce = sodium_base642bin($user['encrypted_store_key_nonce'], SODIUM_BASE64_VARIANT_ORIGINAL);
            $store_key = sodium_crypto_secretbox_open($encrypted_store_key, $encrypted_store_key_nonce, $derived_key);
            if ($store_key === FALSE) {
                http_response_code(500);
                // TODO: serve server error page
                return;
            }
            sodium_memzero($_REQUEST['pass']);
            sodium_memzero($derived_key);

            $_SESSION[Config\SESSION_USER] = $_REQUEST['user'];
            $_SESSION[Config\SESSION_STORE_KEY] = $store_key;
            header('Location: index.php');
            exit();
        }
    }
    $unsuccessful_login_attempt = TRUE;
}

$template = new Template('src/templates/login.php');
$template->login_failed = $unsuccessful_login_attempt;
echo $template->render();