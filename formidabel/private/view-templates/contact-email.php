<html>
<head></head>
<body>
    <h2>Form-ID: <?= html($t_sub['formid'])?></h2>
    <h3>Time: <?= html($t_sub['time']) ?></h3>
    <h3>Address: <?= html($t_sub['addr']) ?></h3>
    <div>
        <?php foreach ($t_sub['params'] as $key => $value) : ?>
        <h3><?= html($key) ?></h3>
        <pre style="font-family: sans-serif; white-space: pre-wrap; word-wrap: break-word;"><?= html($value) ?></pre>
        <?php endforeach; ?>
    </div>
</body>