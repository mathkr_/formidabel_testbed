<?php
require_once('src/init.php');
require_once('src/template.php');

session_start();
if (!isset($_SESSION[Config\SESSION_USER]) || !isset($_SESSION[Config\SESSION_STORE_KEY])) {
    header('Location: ' . Config\LOGIN_PATH);
    exit();
}

if (empty($_GET['id']) || empty($_GET['formid'])) {
    header('Location: index.php');
    exit();
}
$submission = get_single_submission($_SESSION[Config\SESSION_STORE_KEY], $_GET['formid'], $_GET['id']);
if (is_null($submission)) {
    // TODO: 404/error page
    http_response_code(404);
    echo '404 Error! Submission not found!';
    exit();
}

$template = new Template('src/templates/dashboard-submission.php');
$template->sub = $submission;
$template->print = TRUE;

// TODO: use print template and show which form the submission is from
// TODO: also use this file for printing one submission and all the submissions from one form

echo $template->render();