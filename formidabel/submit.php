<?php
require_once('src/init.php');
require_once('src/forms.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $datetime = date_create('@' . $_SERVER['REQUEST_TIME']);
    $datetime = $datetime ? $datetime : date_create();

    // TODO: log each successful and unsuccessful request (add metadata and error information here and remove redundant info from forms.php error messages)
    try {
        if (empty($_POST['_formid'])) {
            throw new Exception('Form id not set in form submission.');
        }
        $form = Form::load_form_from_file($_POST['_formid']);
        $form->process_request($_POST, $datetime, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_HOST']);
    } catch (Exception $e) {
        error_log('Form submission could not be processed: ' . $e);

        $response_code = (bool)$e->getCode() ? $e->getCode() : 500;
        http_response_code($response_code);
        exit();
    }
}