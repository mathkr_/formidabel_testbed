<?php
require_once('config.php');

if (Config\DEBUG) {
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    ini_set('log_errors', 1);
    ini_set('log_startup_errors', 1);
    ini_set('error_log', 'private/debug-error-log.txt');
} else {
    ini_set('error_reporting', E_ALL & ~E_DEPRECATED);
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    ini_set('log_errors', 1);
    ini_set('log_startup_errors', 0);
}

require_once('functions.php');