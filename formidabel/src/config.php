<?php
namespace Config;

// File paths are relative to backend root directory
const TIMESTAMP_SECRET_KEY_FILE = 'private/timestamp_secret_key';
const TIMESTAMP_LAST_N_FILE     = 'private/timestamps_last_n';
const STORE_DIR                 = 'private/store';
const VIEW_TEMPLATE_DIR         = 'private/view-templates';
const USERS_FILE                = 'private/users.json';
const FORMS_FILE                = 'private/forms.json';
const STORE_PUBLIC_KEY_FILE     = 'private/store_public_key';

const LOGIN_PATH                = 'login.php';

const PAGE_LEN                  = 4;

const PWHASH_OPSLIMIT           = SODIUM_CRYPTO_PWHASH_OPSLIMIT_MODERATE;
const PWHASH_MEMLIMIT           = SODIUM_CRYPTO_PWHASH_MEMLIMIT_MODERATE;

const JSON_ENCODE_FLAGS         = (JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

const SESSION_USER              = 'user';
const SESSION_STORE_KEY         = 'store_key';

const UID_LEN                   = 20;

const TIMESTAMP_TIMEOUT_SECONDS = 30.0;
const TIMESTAMP_REQUEST_PERIOD  = 20.0;
const TIMESTAMP_LAST_TOKENS_N   = 128;

const DEBUG                     = TRUE;