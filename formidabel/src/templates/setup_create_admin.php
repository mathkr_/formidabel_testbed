<p class="setup-status">feature-check > <em>create admin</em> > done!</p>

<form method="post">
    <h3>Create Administrator</h3>
    <label>
        Username:<br>
        <input type="text" name="admin_user" autocomplete="off"><br>
    </label>
    <br>
    <label>
        Password:<br>
        <input type="password" name="admin_pass" autocomplete="new-password"><br>
    </label>
    <input type="submit" value="Create Administrator">
<?php if (!empty($t_msg)): ?>
    <div style="color: red;">
        <p><?= html($t_msg) ?></p>
    </div>
<?php endif; ?>
</form>