<?php require_once('../functions.php'); ?>

<div class="subm">
    <h3>Time: <?= html($t_sub['time']) ?></h3>
    <h3>Address: <?= html($t_sub['addr']) ?></h3>
    <div class="subm-content">
        <?php foreach ($t_sub['params'] as $key => $value) : ?>
        <h3><?= html($key) ?></h3>
        <pre style="font-family: sans-serif; white-space: pre-wrap; word-wrap: break-word;"><?= html($value) ?></pre>
        <?php endforeach; ?>
    </div>
    <?php if (!isset($t_print) || !$t_print): ?>
    <a href="print.php?formid=<?= html($t_sub['formid']) ?>&id=<?= html($t_sub['id']) ?>">Print</a>
    <form method="post">
        <input type="hidden" name="fn" value="delete_submission" />
        <input type="hidden" name="id" value="<?= html($t_sub['id']) ?>" />
        <input type="hidden" name="formid" value="<?= html($t_sub['formid']) ?>" />
        <input type="submit" value="Delete" />
    </form>
    <?php endif; ?>
</div>