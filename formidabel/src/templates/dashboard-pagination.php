<?php require_once('../functions.php'); ?>
<a href="?formid=<?= html($t_formid) ?>&page=<?= html($t_page > 1 ? $t_page - 1 : 1) ?>" class="page-link-prev">&lt;</a>
<?php
function page_link($formid, $current_page, $page_num) {
?>
<a class="page-link<?= ($current_page == $page_num ? ' page-link-current' : '') ?>" href="?formid=<?= html($formid) ?>&page=<?= html($page_num) ?>">
    <?= html($page_num) ?>
</a>
<?php
}
$ellipsis = '<span>&hellip;</span>';

if ($t_num_pages <= 5) {
    for ($i = 1; $i <= $t_num_pages; $i++) {
        echo page_link($t_formid, $t_page, $i);
    }
} else {
    if ($t_page - 1 <= 2) {
        // We're near the start
        for ($i = 1; $i <= $t_page + 1; $i++) {
            echo page_link($t_formid, $t_page, $i);
        }
        echo $ellipsis;
        echo page_link($t_formid, $t_page, $t_num_pages);
    } elseif ($t_num_pages - $t_page <= 2) {
        // We're near the end
        echo page_link($t_formid, $t_page, 1);
        echo $ellipsis;
        for ($i = $t_page - 1; $i <= $t_num_pages; $i++) {
            echo page_link($t_formid, $t_page, $i);
        }
    } else {
        // We're somewhere in the middle
        echo page_link($t_formid, $t_page, 1);
        echo $ellipsis;
        for ($i = $t_page - 1; $i <= $t_page + 1; $i++) {
            echo page_link($t_formid, $t_page, $i);
        }
        echo $ellipsis;
        echo page_link($t_formid, $t_page, $t_num_pages);
    }
}
?>
<a href="?formid=<?= html($t_formid) ?>&page=<?= html($t_page < $t_num_pages ? $t_page + 1 : $t_num_pages) ?>" class="page-link-next">&gt;</a>