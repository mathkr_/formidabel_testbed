<?php
require_once('../template.php');

$pagination_template = new Template('dashboard-pagination.php');
$pagination_template->formid    = $t_form['formid'];
$pagination_template->page      = $t_form['page'];
$pagination_template->num_pages = $t_form['num_pages'];
$pagination_str = $pagination_template->render();

?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Formidabel Backend</title>
    <link rel="stylesheet" href="normalize.css">

    <style>
        body {
            width: 100%;
            font-family: Sans-Serif;
        }

        .subm,
        .dashboard,
        .form-list,
        .info-textbox,
        .pagination-controls {
            width: 60%;
            margin: 1em auto;
            padding: 1em;
            background-color: lightgrey;
        }

        .pagination-controls {
            text-align: center;
        }

        .pagination-controls>*,
        .form-list>*,
        .dashboard>* {
            margin: auto 0.5em;
        }

        .pagination-controls .page-link {
            color: darkslategray;
        }

        .pagination-controls .page-link-current {
            font-weight: bolder;
            text-decoration: none;
        }

        .subm-content {
            margin-left: 2em;
            background-color: white;
            padding: 1em;
            border-left: 1px solid grey;
            margin-bottom: 1em;
        }
    </style>
</head>

<body>
    <div class="dashboard">
        <span class="dash-hello">Welcome, <?= isset($t_user) ? $t_user : 'Unknown' ?>!</span>
        <a class="dash-logout" href="logout.php">Logout</a>
        <a class="dash-print-all" href="#">Print All</a>
        <a class="dash-archive-all" href="#">Download Archive</a>
        <a class="dash-delete-all" href="#">Delete All</a>
    </div>

<?php if (!is_null($t_info_text)): ?>
    <div class="info-textbox">
        <p><?= html($t_info_text) ?></p>
    </div>
<?php endif; ?>

    <div class="form-list">
        <?php foreach ($t_form_definitions as $def) : ?>
            <a href="?formid=<?= html($def['id']) ?>&page=1" class="<?= $def['id'] === $t_form['formid'] ? 'form-link-current' : '' ?>">
                <?= html($def['name']) ?>
            </a>
        <?php endforeach; ?>
    </div>

    <div class="form-content">
        <div class="pagination-controls">
            <?= $pagination_str ?>
        </div>
        <div id="subm-list">
<?php
if (!empty($t_form['submissions'])) {
    $sub_template = new Template('dashboard-submission.php');
    foreach ($t_form['submissions'] as $sub) {
        $sub_template->sub = $sub;
        echo $sub_template->render();
    }
} else {
    echo '<div class="subm"><p>No submissions.</p></div>';
}
?>
        </div>
        <div class="pagination-controls">
            <?= $pagination_str ?>
        </div>
    </div>
</body>

</html>