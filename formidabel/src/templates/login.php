<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Formidabel Backend</title>
    <link rel="stylesheet" href="normalize.css">

    <style>
        body {
            width: 100%;
            font-family: Sans-Serif;
        }

        #login-form,
        #feedback-box {
            width: 320px;
            margin: 1em auto;
            padding: 1em;
            background-color: lightgrey;
        }

        #feedback-box {
            text-align: center;
        }

        #login-form input {
            margin-bottom: 0.5em;
        }

        #login-form input[type=text],
        #login-form input[type=password] {
            width: 100%;
        }
    </style>
</head>

<body>
    <form method="post" id="login-form">
        <label for="login-input-user">Username:</label><br>
        <input type="text" id="login-input-user" name="user"><br>
        <label for="login-input-pass">Password:</label><br>
        <input type="password" id="login-input-pass" name="pass"><br>
        <input type="hidden" name="fn" value="login">
        <input type="submit" value="Login">
    </form>
<?php if ($t_login_failed): ?>
    <div id="feedback-box">
        <p>Invalid username or password.</p>
    </div>
<?php endif; ?>
</body>