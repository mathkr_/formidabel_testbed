<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Formidabel Setup</title>
    <link rel="stylesheet" href="normalize.css">

    <style>
        body {
            width: 100%;
            font-family: Sans-Serif;
        }
        .setup-wrapper {
            width: 600px;
            margin: 1em auto;
            padding: 1em;
            background-color: #DDDDDD;
        }
        form input {
            margin-bottom: 0.5em;
        }
        form input[type=text],
        form input[type=password] {
            width: 100%;
        }
        .setup-status {
            text-align: center;
            font-size: 0.9em;
        }
        .setup-status em {
            font-weight: bold;
        }
    </style>
</head>
<body>
        <div class="setup-wrapper">