<?php

class Template {
    private $template;
    private $params = [];

    public function __construct($template_file = NULL) {
        $this->template = $template_file;
    }

    public function set_template(string $template_file) {
        $this->template = $template_file;
    }

    public function __get($name) {
        return $this->params[$name];
    }

    public function __set($name, $value) {
        $this->params[$name] = $value;
    }

    public function render(): string {
        if (is_null($this->template)) {
            throw new Exception('Template file not set in template');
        }

        $original_dir = getcwd();
        extract($this->params, EXTR_PREFIX_ALL, 't');
        chdir(dirname($this->template));
        ob_start();
        include basename($this->template);
        $res = ob_get_clean();
        chdir($original_dir);
        return $res;
    }
}