<?php
require_once('init.php');

function match_request(string $method, array $required_params = []): bool {
    if ($method !== $_SERVER['REQUEST_METHOD']) {
        return FALSE;
    }
    $req = $_REQUEST;
    switch ($method) {
        case 'GET':
            $req = $_GET;
            break;
        case 'POST':
            $req = $_POST;
            break;
    }
    foreach ($required_params as $key => $val) {
        if (empty($req[$key]) || (!is_null($val) && $req[$key] != $val)) {
            return FALSE;
        }
    }
    return TRUE;
}

function my_htmlentities(string $str): string {
    return htmlentities($str, ENT_QUOTES|ENT_HTML5);
}

function my_htmlspecialchars(string $str): string {
    return htmlspecialchars($str, ENT_QUOTES|ENT_HTML5);
}

// TODO: only need this one now?
function html(?string $str): string {
    return htmlspecialchars($str, ENT_QUOTES|ENT_HTML5);
}

// function concat_paths(array $segments) {
//     $segments = str_replace('\\', '/', $segments);
//     $path = '';
//     // TODO: finish me
// }

function my_htmlspecialchars_array(array $arr): array {
    $res = array();

    foreach ($arr as $key => $val) {
        if (is_array($val)) {
            $res[my_htmlspecialchars($key)] = my_htmlspecialchars_array($val);
        } else if (is_scalar($val)) {
            // scalar types should get coerced to string
            $res[my_htmlspecialchars($key)] = my_htmlspecialchars($val);
        } else {
            throw new Exception('Unexpected type in my_htmlspecialchars_array');
        }
    }

    return $res;
} 

function clamp($num, $min, $max) {
    if ($num < $min) {
        return $min;
    }
    if ($num > $max) {
        return $max;
    }
    return $num;
}

function get_form_definitions() {
    $forms = json_decode(file_get_contents(Config\FORMS_FILE), TRUE);
    if (is_null($forms)) {
        throw new Exception('Cannot load and decode form definition file.');
    }
    foreach ($forms as $formid => $formdef) {
        $forms[$formid]['id'] = $formid; 
    }
    return $forms;
}

function get_single_submission(string $store_secret_key, string $formid, string $id) {
    // TODO: can this be refactored with get_form_data (and also include get_all for print all submissions).
    $store_public_key = sodium_crypto_box_publickey_from_secretkey($store_secret_key);
    $store_keypair = sodium_crypto_box_keypair_from_secretkey_and_publickey($store_secret_key, $store_public_key);

    $file = Config\STORE_DIR . '/' . $formid . '/' . $id;
    if (!is_file($file)) {
        return NULL;
    }
    $encrypted = file_get_contents($file);
    $json = sodium_crypto_box_seal_open($encrypted, $store_keypair);
    // TODO: any use for checking if box seal open failed? what's are the ways it could?
    $submission = json_decode($json, TRUE);
    return $submission;
}

function get_form_data(string $store_secret_key, string $formid, int $page) {
    $store_public_key = sodium_crypto_box_publickey_from_secretkey($store_secret_key);
    $store_keypair = sodium_crypto_box_keypair_from_secretkey_and_publickey($store_secret_key, $store_public_key);

    $files = array_filter(
        scandir(Config\STORE_DIR . '/' . $formid . '/', SCANDIR_SORT_DESCENDING),
        function(string $filename): bool {
            if (strpos($filename, '.') === 0) {
                return false;
            }
            return true;
        }
    );

    $num_pages = ceil(count($files) / Config\PAGE_LEN);
    $num_pages = $num_pages == 0 ? 1 : $num_pages;
    $page_index = clamp($page - 1, 0, $num_pages - 1);

    $submissions = array();
    if (!empty($files)) {
        for ($i = $page_index * Config\PAGE_LEN; $i < min(($page_index * Config\PAGE_LEN) + Config\PAGE_LEN, count($files)); $i++) {
            $encrypted = file_get_contents(Config\STORE_DIR . '/' . $formid . '/' . $files[$i]);
            $json = sodium_crypto_box_seal_open($encrypted, $store_keypair);
            // TODO: any use for checking if box seal open failed? what's are the ways it could?
            $submission = json_decode($json, TRUE);
            $submissions[] = $submission;
        }
    }

    return [
        'page'                  => $page_index + 1,
        'num_pages'             => $num_pages,
        'num_submissions_total' => count($files),
        'formid'                => $formid,
        'submissions'           => $submissions,
    ];
}

// TODO: switch to something like twitters snowflake id?
function timestamped_uid(DateTime $datetime, int $length): string {
    return $datetime->format('Ymd-His') . '--' . bin2hex(random_bytes(ceil($length / 2)));
}

/**
 * Responds to the http request and returns execution to the script without making the
 * user wait for the request to finish.
 * You may want to set_time_limit if you anticipate the work do take a long time.
 */
function finish_request_ok(string $output): void {
    ob_start();
    echo $output;

    header('Content-Encoding: none');
    ignore_user_abort(true);
    if (session_id() === '') {
        session_write_close();
    }
    if (is_callable('fastcgi_finish_request')) {
        fastcgi_finish_request();
    } else {
        header($_SERVER['SERVER_PROTOCOL'] .' 200 OK');
        header('Content-Length: ' . ob_get_length());
        @ob_end_flush();
        @ob_flush();
        @flush();
    }
}

/**
 * @throws Exception if something goes wrong
 */
function initial_setup_with_admin(string $admin_user, string $admin_pass) {
    // Generating timestamp secret key and writing to file..
    $timestamp_key = sodium_crypto_secretbox_keygen();
    if (file_put_contents(Config\TIMESTAMP_SECRET_KEY_FILE, $timestamp_key) === FALSE) {
        throw new Exception('Could not write timestamp secret key to disk');
    }

    // Generating store keypair..
    $store_keypair = sodium_crypto_box_keypair();
    $store_public_key = sodium_crypto_box_publickey($store_keypair);
    $store_secret_key = sodium_crypto_box_secretkey($store_keypair);

    // Writing store public key to file..
    if (file_put_contents(Config\STORE_PUBLIC_KEY_FILE, $store_public_key) === FALSE) {
        throw new Exception('Could not write store public key to disk');
    }

    $users = [
        $admin_user => $admin_pass,
    ];

    // Encrypting store key per user with derived key..
    foreach ($users as $user => $pass) {
        $derived_key_salt = random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
        $derived_key_len = SODIUM_CRYPTO_SECRETBOX_KEYBYTES;
        $derived_key = sodium_crypto_pwhash(
            $derived_key_len,
            $pass,
            $derived_key_salt,
            Config\PWHASH_OPSLIMIT,
            Config\PWHASH_MEMLIMIT
        );

        $encrypted_store_key_nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $encrypted_store_key = sodium_crypto_secretbox(
            $store_secret_key,
            $encrypted_store_key_nonce,
            $derived_key
        );

        $users[$user] = [
            'pwhash'                    => sodium_crypto_pwhash_str($pass, Config\PWHASH_OPSLIMIT, Config\PWHASH_MEMLIMIT),
            'derived_key_salt'          => sodium_bin2base64($derived_key_salt, SODIUM_BASE64_VARIANT_ORIGINAL),
            'encrypted_store_key'       => sodium_bin2base64($encrypted_store_key, SODIUM_BASE64_VARIANT_ORIGINAL),
            'encrypted_store_key_nonce' => sodium_bin2base64($encrypted_store_key_nonce, SODIUM_BASE64_VARIANT_ORIGINAL),
        ];
    }

    // Writing users file..
    if (file_put_contents(Config\USERS_FILE, json_encode($users, Config\JSON_ENCODE_FLAGS)) === FALSE) {
        throw new Exception('Could not write users file to disk');
    }
}