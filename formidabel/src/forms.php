<?php

namespace Form {
    const RESERVED_PARAMS = [
        '_formid',
    ];

    const EMAIL_ACTION_PARAM_WHITELIST = [
        'type',
        'recipients',
        'recipient_params',
        'from',
        'replyto_params',
        'template_html',
        'template_plaintext',
        'template_subject',
        'server',
    ];
}

namespace {

require_once('init.php');
require_once('template.php');

require_once('thirdparty/PHPMailer/PHPMailer.php');
require_once('thirdparty/PHPMailer/SMTP.php');
require_once('thirdparty/PHPMailer/Exception.php');
use PHPMailer\PHPMailer\PHPMailer;

abstract class FormAction {
    /**
     * @throws Exception if no valid action can be created from $definition
     * @return FormAction
     */
    static function make(array $definition, Form $form): FormAction {
        if (empty($definition['type'])) {
            throw new Exception("Form action type not set ({$form->id}).");
        }
        $className = 'FormAction' . $definition['type'];
        if (!class_exists($className, FALSE)) {
            throw new Exception('Invalid form action type: ' . $definition['type']);
        }
        return new $className($definition, $form);
    }

    public function counts_as_submitted(): bool {
        return TRUE;
    }

    abstract public function __construct(array $definition, Form $form);
    
    /**
     * @throws Exception if the action could not be successfully executed
     */
    abstract public function execute(array $submission);
}

class FormActionStore extends FormAction {
    protected $form;

    public function __construct(array $definition, Form $form) {
        $this->form = $form;
    }

    public function execute(array $submission) {
        $json = json_encode($submission, Config\JSON_ENCODE_FLAGS);
        $encrypted = $this->form->encrypt($json);

        $dir = $this->form->get_store_dir();
        $umask = umask(0002); // We want to set the group-write bit, so only disallow others-write
        if (!is_dir($dir) && !mkdir($dir, 0770, TRUE)) {
            throw new Exception('Store dir could not be created: ' . $dir);
        }
        umask($umask);

        $put_result = file_put_contents($dir . '/' . $submission['id'], $encrypted);
        if ($put_result === FALSE) {
            throw new Exception('Could not write submission to store. Dir: ' . $dir . ' File: ' . $submission['id']);
        }
    }
}

class FormActionEmail extends FormAction {
    protected $form;

    public function __construct(array $definition, Form $form) {
        $this->form = $form;

        foreach ($definition as $key => $val) {
            if (in_array($key, Form\EMAIL_ACTION_PARAM_WHITELIST)) {
                $this->$key = $val;
            } else {
                error_log("Unexpected parameter in email form action definition ($form->id): $key => $val");
            }
        }

        $this->assert_is_set(isset($this->from), 'action->from');
        $this->assert_is_set(isset($this->server), 'action->server');
        $this->assert_is_set(isset($this->server['host']), 'action->server->host');
        $this->assert_is_set(isset($this->server['port']), 'action->server->port');
        $this->assert_is_set(isset($this->server['transport_security']), 'action->server->transport_security');
        if (isset($this->server['user'])) {
            $this->assert_is_set(isset($this->server['password']), 'action->server->password');
        }
    }

    protected function assert_is_set($expr, $prop_name) {
        if (!(bool)$expr) {
            throw new Exception("Incomplete email form action definition ({$this->form->id}): Missing: $prop_name");
        }
    }

    protected function render_message(array $submission): array {
        $result = array();
        foreach (['html', 'plaintext', 'subject'] as $type) {
            $prop_name = 'template_' . $type;
            if (isset($this->$prop_name)) {
                $template = new Template(Config\VIEW_TEMPLATE_DIR . '/' . $this->$prop_name);
                $template->sub = $submission;
                $result[$type] = $template->render();
            } else {
                error_log("No template defined in email form action definition ({$this->form->id}): $type (Property: $prop_name)");
            }
        }
        return $result;
    }

    /**
     * @throws Exception when the mail(s) could not be sent successfully or any errors occured during the sending of them
     */
    protected function send_mail(array $recipients, array $submission, ?array $replyto = NULL) {
        // TODO: be able to show see PHPMailer debug info somehow?

        $mail = new PHPMailer(TRUE);

        $mail->CharSet = 'utf-8';
        $mail->Encoding = 'base64';

        $mail->XMailer = NULL;

        $mail->isSMTP();
        $mail->Host = $this->server['host'];
        $mail->Port = $this->server['port'];

        if (isset($this->server['user'])) {
            $mail->SMTPAuth = TRUE;
            $mail->Username = $this->server['user'];
            $mail->Password = $this->server['password'];
        }

        $mail->SMTPAutoTLS = TRUE;
        switch (strtolower($this->server['transport_security'])) {
            case 'starttls':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                break;
            case 'smtps':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                break;
            case 'none':
                $mail->SMTPSecure = '';
                break;
            default:
                throw new Exception("Invalid transport security method in form action definition ({$this->form->id}): {$this->server['transport_security']}");
        }

        $mail->setFrom($this->from[0], empty($this->from[1]) ? '' : $this->from[1]);

        $any_recipients_valid = FALSE;
        foreach ($recipients as $recipient) {
            if (empty($recipient[0])) {
                continue;
            }
            try {
                if ($mail->addAddress($recipient[0], empty($recipient[1]) ? '' : $recipient[1])) {
                    $any_recipients_valid = TRUE;
                } else {
                    error_log("Duplicate recipient in send_mail execution ({$this->form->id}): " . print_r($recipient, TRUE));
                }
            } catch (Exception $e) {
                error_log("Invalid recipient in send_mail execution ({$this->form->id}): " . print_r($recipient, TRUE));
            }
        }
        if (!$any_recipients_valid) {
            throw new Exception("No valid recipients in form action send_mail execution ({$this->form->id})");
        }

        if (!is_null($replyto)) {
            $mail->addReplyTo($replyto[0], empty($replyto[1]) ? '' : $replyto[1]);
        }

        $message = $this->render_message($submission);
        if (empty($message['html']) && empty($message['plain'])) {
            throw new Exception('No message could be rendered from submission in form action send_mail execution (' . $this->form->id . ')');
        }
        if (!empty($message['html'])) {
            $mail->isHTML(TRUE);
            $mail->Body = $message['html'];
            // TODO: may use $mail->msgHTML to automatically inline images and other stuff PHPMailer may do for us
        }
        if (!empty($message['plaintext'])) {
            $mail->AltBody = $message['plaintext'];
        }
        $mail->Subject = isset($message['subject']) ? $message['subject'] : 'No Subject';
        $mail->send();
    }

    public function execute(array $submission) {
        $replyto = NULL;
        if (!empty($this->replyto_params)) {
            $replyto[] = $submission['params'][$this->replyto_params[0]];
            $replyto[] = empty($this->replyto_params[1]) ? NULL : $submission['params'][$this->replyto_params[1]];
        }
        $this->send_mail($this->recipients, $submission, $replyto);
    }
}

class FormActionEmailResponse extends FormActionEmail {
    public function counts_as_submitted(): bool {
        return FALSE;
    }

    public function execute(array $submission) {
        if (empty($this->recipient_params)) {
            throw new Exception('Recipient param names not specified in form action email response definition.');
        }
        $recipient = [
            $submission['params'][$this->recipient_params[0]],
            empty($this->recipient_params[1]) ? NULL : $submission['params'][$this->recipient_params[1]]
        ];
        $this->send_mail([$recipient], $submission);
    }
}

abstract class AntiSpam {
    static function make(array $definition, Form $form): AntiSpam {
        if (empty($definition['type'])) {
            throw new Exception("No type set in anti-spam measure definition");
        }
        switch ($definition['type']) {
            case 'grecaptcha_v2':
                return new AntiSpamGrecaptchaV2($definition, $form);
            case 'basic':
                return new AntiSpamBasic($definition, $form);
            case 'javascript':
                return new AntiSpamJS($definition, $form);
            default:
                throw new Exception('Invalid type set in anti-spam measure definition');
        }
    }

    abstract public function __construct(array $definition, Form $form);

    /**
     * @throws Exception if the anti-spam measure wasn't passed successfully
     */
    abstract public function verify(array $raw_request, string $remote_addr, string $raw_http_host);
}

class AntiSpamGrecaptchaV2 extends AntiSpam {
    private $secret_key;
    public function __construct(array $definition, Form $form) {
        if (empty($definition['secret_key'])) {
            throw new Exception('Google recaptcha v2 secret key not set in anti-spam measure definition');
        }
        $this->secret_key = $definition['secret_key'];
        $form->add_ignore_param('g-recaptcha-response');
    }

    public function verify(array $raw_request, string $remote_addr, string $raw_http_host) {
        if (empty($raw_request['g-recaptcha-response'])) {
            throw new Exception('Google recaptcha v2 response parameter required', 401);
        }
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL             => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_POST            => TRUE,
            CURLOPT_RETURNTRANSFER  => TRUE,
            CURLOPT_HEADER          => FALSE,
            CURLOPT_POSTFIELDS      => [
                'secret'    => $this->secret_key,
                'response'  => $raw_request['g-recaptcha-response'],
                'remoteip'  => $remote_addr
            ],
        ]);
        $result = curl_exec($curl);
        if ($result === FALSE) {
            throw new Exception('Curl error while trying to verify Google recaptcha v2 response: ' . curl_error($curl), 500);
        }
        $response = json_decode($result, TRUE);
        if (is_null($response)) {
            throw new Exception('Could not decode Google recaptcha v2 verification response', 500);
        } 
        if (!isset($response['success']) || $response['success'] !== TRUE) {
            $err = isset($response['error-codes']) ? print_r($response['error-codes'], TRUE) : NULL;
            throw new Exception('Could not verify Google recaptcha v2 response: ' . $err, 401);
        }
    }
}

class AntiSpamBasic extends AntiSpam {
    private $honeypot_field;
    private $allowed_hosts;
    public function __construct(array $definition, Form $form) {
        if (empty($definition['honeypot_field'])) {
            throw new Exception('Basic honeypot_field param_name not set', 500);
        }
        $this->honeypot_field = $definition['honeypot_field'];
        if (empty($definition['allowed_hosts'])) {
            throw new Exception('Allowed hosts not set in basic anti spam definition');
        }
        $this->allowed_hosts = [];
        foreach ($definition['allowed_hosts'] as $host) {
            $canonical_host = $this->canonicalize_host($host);
            if (is_null($canonical_host)) {
                error_log('Invalid allowed host in basic anti spam definition: ' . $host);
            }
            $this->allowed_hosts[] = $canonical_host;
        }
        if (!count($this->allowed_hosts)) {
            throw new Exception('No valid allowed hosts set in basic anti spam definition');
        }
        $form->add_ignore_param($this->honeypot_field);
    }

    /**
     * @return string|NULL either returns the canonicalized host or NULL if the url was seriously malformed
     */
    private function canonicalize_host(string $raw_host): ?string {
        $path = parse_url($raw_host, PHP_URL_PATH);
        if (!(bool)$path) {
            return NULL;
        }
        $path = mb_strtolower($path, 'UTF-8');
        if (strpos($path, 'www.') === 0) {
            $path = substr($path, 4);
        }
        return $path;
    }

    public function verify(array $raw_request, string $remote_addr, string $raw_http_host) {
        if (!empty($raw_request[$this->honeypot_field])) {
            throw new Exception('Basic honeypot was not empty', 401);
        }
        if (!isset($raw_request[$this->honeypot_field])) {
            throw new Exception('Basic honeypot field is missing from request, should get send but empty', 401);
        }
        $canonical_host = $this->canonicalize_host($raw_http_host);
        if (is_null($canonical_host) || !in_array($canonical_host, $this->allowed_hosts)) {
            throw new Exception('Request http_host not allowed in basic anti spam definition: ' . $canonical_host, 401);
        }
    }
}

class AntiSpamJS extends AntiSpam {
    private $min_submit_time;
    private $timestamp_secret_key;
    public function __construct(array $definition, Form $form) {
        if (empty($definition['min_submit_time'])) {
            throw new Exception('min_submit_time not set in javascript anti-spam measure definition');
        }
        if (!is_numeric($definition['min_submit_time'])) {
            throw new Exception('min_submit_time should be the number of seconds');
        }
        $this->min_submit_time = floatval($definition['min_submit_time']);

        $this->timestamp_secret_key = file_get_contents(Config\TIMESTAMP_SECRET_KEY_FILE);
        if ($this->timestamp_secret_key === FALSE) {
            throw new Exception('Could not load timestamp secret key file');
        }
        $form->add_ignore_param('_submit_token');
    }

    public function verify(array $raw_request, string $remote_addr, string $raw_http_host) {
        if (empty($raw_request['_submit_token'])) {
            throw new Exception('Missing _submit_token field in request', 400);
        }

        $fragments = explode(':', $raw_request['_submit_token']);
        if ($fragments === FALSE || count($fragments) != 2) {
            throw new Exception('Malformed _submit_token received', 400);
        }
        $timestamp = sodium_crypto_secretbox_open(
            sodium_base642bin($fragments[0], SODIUM_BASE64_VARIANT_ORIGINAL),
            sodium_base642bin($fragments[1], SODIUM_BASE64_VARIANT_ORIGINAL),
            $this->timestamp_secret_key
        );
        if ($timestamp === FALSE) {
            throw new Exception('Invalid _submit_token received', 401);
        }

        $age = microtime(TRUE) - floatval($timestamp);
        if ($age < $this->min_submit_time) {
            // TODO: throw some kind of end user facing error message
            throw new Exception("Form request was submitted too fast: min-time={$this->min_submit_time}s, age={$age}s", 401);
        } else if ($age >= Config\TIMESTAMP_TIMEOUT_SECONDS) {
            // TODO: throw some kind of end user facing error message
            throw new Exception('Timed out _submit_token: max-time=' . Config\TIMESTAMP_TIMEOUT_SECONDS . "s, age={$age}s", 401);
        } else {
            // compare and log last N used tokens
            $timestamp_last_n_file = file_get_contents(Config\TIMESTAMP_LAST_N_FILE);
            if ($timestamp_last_n_file == FALSE) {
                error_log('Could not read timestamp_last_n file, trying to create one');
                if (file_put_contents(Config\TIMESTAMP_LAST_N_FILE, $fragments[0], LOCK_EX) === FALSE) {
                    error_log('Could not write timestamp_last_n file');
                }
            } else {
                $invalidated_timestamps = explode("\n", $timestamp_last_n_file);
                if (in_array($fragments[0], $invalidated_timestamps)) {
                    throw new Exception('Invalidated _submit_token received again', 401);
                } else {
                    $invalidated_timestamps[] = $fragments[0];
                    $count = count($invalidated_timestamps);
                    if ($count > Config\TIMESTAMP_LAST_TOKENS_N) {
                        $offset = $count - Config\TIMESTAMP_LAST_TOKENS_N;
                        $invalidated_timestamps = array_slice($invalidated_timestamps, $offset);
                    }
                    if (file_put_contents(Config\TIMESTAMP_LAST_N_FILE, implode("\n", $invalidated_timestamps), LOCK_EX) === FALSE) {
                        error_log('Could not write timestamp_last_n file');
                    }
                }
            }
        }
    }
}

class Form {
    public $id;

    private $definition;
    private $store_public_key;
    private $actions;
    private $anti_spam_measures;
    private $ignore_params;

    /**
     * @return  Form
     * @throws  Exception if the form file doesn't exist, could not be parsed, isn't set up correctly or doesn't include the requested form id
     */
    static public function load_form_from_file(string $formid): Form {
        $definitions = json_decode(file_get_contents(Config\FORMS_FILE), TRUE);
        if (is_null($definitions)) {
            throw new Exception('Could not load form definitions from form file: ' . Config\FORMS_FILE, 500);
        } else if (!array_key_exists($formid, $definitions)) {
            throw new Exception('Requested form id not found: ' . mb_strimwidth($formid, 0, 20, '...'), 404);
        }
        $definition = $definitions[$formid];

        $store_public_key = file_get_contents(Config\STORE_PUBLIC_KEY_FILE);
        if ($store_public_key === FALSE) {
            error_log('Store public key could not be loaded from file: ' . Config\STORE_PUBLIC_KEY_FILE);
            // NOTE: This means that any store actions won't succeed,
            // but we should still try to execute the others (sending by mail etc.).
            $store_public_key = NULL;
        }

        return new Form($formid, $definition, $store_public_key);
    }

    /**
     * @throws Exception when the form includes no or only invalid action definitions
     */
    public function __construct(string $id, array $definition, ?string $store_public_key) {
        $this->id = $id;
        $this->definition = $definition;
        $this->store_public_key = $store_public_key;

        $this->anti_spam_measures = array();
        foreach ($definition['anti_spam_measures'] as $anti_spam_definition) {
            $this->anti_spam_measures[] = AntiSpam::make($anti_spam_definition, $this);
        }

        $this->actions = array();
        foreach ($definition['actions'] as $action_definition) {
            try {
                $this->actions[] = FormAction::make($action_definition, $this);
            } catch (Exception $e) {
                error_log('Could not create form action from definition in form "' . $this->id . '": ' . $e);
            }
        }
        if (!count($this->actions)) {
            throw new Exception("No valid actions defined in form definition ($id)", 500);
        }
    }

    /**
     * @throws Exception if the request is rejected or could not be processed in any meaningful way
     */
    public function process_request(array $raw_request, DateTime $request_time, string $remote_addr, string $raw_http_host) {
        foreach ($this->anti_spam_measures as $measure) {
            $measure->verify($raw_request, $remote_addr, $raw_http_host);
        }

        $params = array();
        foreach ($raw_request as $key => $val) {
            if (in_array($key, Form\RESERVED_PARAMS) || in_array($key, $this->ignore_params)) {
                continue;
            } else if (($filtered_param = $this->filter_param($key, $val)) !== NULL) {
                $params[$key] = $filtered_param;
            } else {
                throw new Exception('Invalid param: ' . mb_strimwidth($key, 0, 16, '...') . ' => ' . mb_strimwidth($val, 0, 16, '...'), 400);
            }
        }
        foreach ($this->definition['params'] as $param_def) {
            if (
                isset($param_def['required']) &&
                filter_var($param_def['required'], FILTER_VALIDATE_BOOLEAN) &&
                empty($params[$param_def['name']])
            ) {
                throw new Exception('Missing required submission parameter: ' . $param_def['name'], 400);
            }
        }

        $submission = [
            'time'      => $request_time->format('Y-m-d H:i:s'),
            'addr'      => $remote_addr,
            'id'        => timestamped_uid($request_time, Config\UID_LEN),
            'formid'    => $this->id,
            'params'    => $params,
        ];

        $submitted = FALSE;
        foreach ($this->actions as $action) {
            try {
                $action->execute($submission);
                $submitted |= $action->counts_as_submitted();
            } catch (Exception $e) {
                error_log("Exception during action execution in form submission ({$this->id}): " . $e);
            }
        }
        if (!$submitted) {
            throw new Exception('No action could be executed successfully for submission in form "' . $this->id . '"', 500);
        }
    }

    /**
     * Add param to ignored in the param validation step. Used by anti spam measures
     * to ignore for example honeypot fields and google recaptcha tokens.
     */
    public function add_ignore_param(string $name): void {
        $this->ignore_params[$name] = $name;
    }

    /**
     * @return string|NULL returns a validated and potentially modified string or NULL if the param was not valid
     */
    public function filter_param(string $name, string $value): ?string {
        $def = NULL;
        foreach ($this->definition['params'] as $param_def) {
            if ($param_def['name'] === $name) {
                $def = $param_def;
                break;
            }
        }
        if (is_null($def)) {
            return NULL;
        }
        $value = trim($value);
        $len = strlen($value);

        if (isset($def['allowed_values'])) {
            if (!is_array($def['allowed_values'])) {
                throw new Exception('Allowed values in param definition is not an array', 500);
            }
            if (!in_array($value, $def['allowed_values'])) {
                return NULL;
            }
        }

        $type = isset($def['type']) ? $def['type'] : 'string';
        switch ($type) {
            case 'tel':
                // Do some very permissive checking to invalidate only obviously bad phone numbers
                $num_digits = intval(preg_match_all('/[0-9]/', $value));
                if (
                    $num_digits < 2 ||
                    $num_digits > 20 ||
                    preg_match('/[^0-9_\-.* +()[]\/]/', $value)
                ) {
                    return NULL;
                }
                break;

            case 'date':
                try {
                    $date_object = new DateTime($value);
                } catch (Exception $e) {
                    return NULL;
                }
                break;
            
            case 'time':
                try {
                    $date_object = new DateTime('01.01.1970 ' . $value);
                    if ($date_object->format('d.m.Y') !== '01.01.1970') {
                        // Check because otherwise the datetime constructor may accept
                        // time values after 23:59 or other such nonsense
                        return NULL;
                    }
                } catch (Exception $e) {
                    return NULL;
                }
                break;

            case 'int':
                $opt = [
                    'options' => [
                        'min_range' => isset($def['min_value']) ? intval($def['min_value']) : PHP_INT_MIN,
                        'max_range' => isset($def['max_value']) ? intval($def['max_value']) : PHP_INT_MAX,
                    ],
                    'flags' => FILTER_NULL_ON_FAILURE
                ];
                if (is_null(filter_var($value, FILTER_VALIDATE_INT, $opt))) {
                    return NULL;
                }
                break;

            case 'num':
            case 'float':
                $opt = [
                    'options' => [
                        'min_range' => isset($def['min_value']) ? floatval($def['min_value']) : -PHP_FLOAT_MAX,
                        'max_range' => isset($def['max_value']) ? floatval($def['max_value']) : PHP_FLOAT_MAX,
                    ],
                    'flags' => FILTER_NULL_ON_FAILURE
                ];
                if (is_null(filter_var($value, FILTER_VALIDATE_FLOAT, $opt))) {
                    return NULL;
                }
                break;

            case 'bool':
                if (is_null(filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE))) {
                    return NULL;
                }
                break;

            case 'email':
                // NOTE 24/03/2021: At the present moment this does not include any sort of
                // non-latin character support (neither in the local part, nor the domain).
                // Since PHPMailer doesn't support international addresses right now, it's what
                // we stick with for the moment.
                if(PHPMailer::validateAddress($value) === FALSE) {
                    return NULL;
                }
                break;
            
            case 'string':
                if (isset($def['min-len']) && $len < intval($def['min-len'])) {
                    return NULL;
                }
                if (isset($def['max-len']) && $len > intval($def['max-len'])) {
                    return NULL;
                }
                break;

            default:
                throw new Exception('Unexpected param type in definition: ' . $type, 500);
        }

        return $value;
    }

    /**
     * @throws Exception when no store_public_key has been previously loaded
     */
    public function encrypt(string $data): string {
        if (is_null($this->store_public_key)) {
            throw new Exception('Trying to encrypt but store_public_key was not loaded');
        }
        return sodium_crypto_box_seal($data, $this->store_public_key);
    }

    public function get_store_dir(): string {
        return Config\STORE_DIR . '/' . $this->id;
    }
}

}