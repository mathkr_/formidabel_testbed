<?php
require_once('src/init.php');
require_once('src/template.php');

session_start();
if (!isset($_SESSION[Config\SESSION_USER]) || !isset($_SESSION[Config\SESSION_STORE_KEY])) {
    header('Location: ' . Config\LOGIN_PATH);
    exit();
}

$template = new Template('src/templates/dashboard.php');
$template->info_text = NULL;

if (match_request("POST", [ 'fn' => 'delete_submission', 'id' => NULL, 'formid' => NULL ])) {
    $file = Config\STORE_DIR . '/' . $_POST['formid'] . '/' . $_POST['id'];
    $template->info_text = is_file($file) && unlink($file)
        ? 'Submission deleted.'
        : 'Could not delete submission.';
}

$form_definitions = get_form_definitions();
$form_data = get_form_data(
    $_SESSION[Config\SESSION_STORE_KEY],
    !empty($_GET['formid']) ? $_GET['formid'] : current($form_definitions)['id'],
    !empty($_GET['page']) ? $_GET['page'] : 1
);

$template->form = $form_data;
$template->form_definitions = $form_definitions;
$template->user = $_SESSION[Config\SESSION_USER];
echo $template->render();