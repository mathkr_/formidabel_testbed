"use strict";

// TODO: only request submit token when focusing any form element?
document.addEventListener("DOMContentLoaded", function() {
    var forms = document.querySelectorAll("form[data-form-submit]");
    for (const form of forms) {
        initForm(form);
    }
    // Adding an unload event to make sure the page gets reloaded after using the back button.
    var noop = function() {};
    window.addEventListener('unload', function(event) { noop(); });
});

function requestFormSubmitToken(url, token_input) {
    var req = new XMLHttpRequest();
    req.open("POST", url, true);
    req.responseType = "json";
    req.onerror = function () {
        console.log("Error during form initialization request");
        setTimeout(function () {
            requestFormSubmitToken(url, token_input);
        }, 10000);
    }
    req.onload = function () {
        token_input.setAttribute('value', req.response.submit_token);
        setTimeout(function () {
            requestFormSubmitToken(url, token_input);
        }, req.response.request_period * 1000);
    };
    req.send();
}

function initForm(form) {
    var url = form.getAttribute('action');
    form.setAttribute('action', form.getAttribute('data-form-submit'));

    var focusin_handler = function(event) {
        form.removeEventListener('focusin', focusin_handler);

        var token_input = document.createElement('input');
        token_input.setAttribute('type', 'hidden');
        token_input.setAttribute('name', '_submit_token');
        token_input.setAttribute('value', '');
        form.appendChild(token_input);

        requestFormSubmitToken(url, token_input);
    };
    form.addEventListener('focusin', focusin_handler, false);
}