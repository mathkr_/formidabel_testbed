<?php
require_once('src/init.php');
require_once('src/template.php');

// TODO: tls or bust
// TODO: treat notices & warnings as fatal?
const SETUP_SESSION = 'setup_state';

set_error_handler(function ($sev, $str, $file, $line) {
    if (!(error_reporting() & $sev)) {
        return;
    }
    $msg = "Error($sev): $str in $file on line $line";
    if ($sev == E_NOTICE || $sev == E_WARNING) {
        throw new ErrorException($msg, 0, $sev, $file, $line);
    } else {
        error_log($msg);
    }
});

// TODO: short lived session
session_start();

if (!array_key_exists(SETUP_SESSION, $_SESSION) && file_exists(Config\USERS_FILE)) {
    header('Location: index.php');
    exit();
}

const SETUP_STATE_FEATURE_CHECK = 1;
const SETUP_STATE_CREATE_ADMIN  = 2;
const SETUP_STATE_DONE          = 3;

if (!isset($_SESSION[SETUP_SESSION])) {
    $_SESSION[SETUP_SESSION] = SETUP_STATE_FEATURE_CHECK;
}

$header = new Template('src/templates/setup_header.php');
$footer = new Template('src/templates/setup_footer.php');
$body = new Template();

if (
    $_SESSION[SETUP_SESSION] == SETUP_STATE_CREATE_ADMIN &&
    match_request('POST', ['admin_user' => NULL, 'admin_pass' => NULL])
) {
    if (preg_match('/[^A-Za-z0-9_]/', $_POST['admin_user'])) {
        $body->msg = 'The username may only contain alphanumeric characters and underscores.';
    } else if (preg_match('/[[:cntrl:]]/', $_POST['admin_pass'])) {
        $body->msg = 'The password may not contain control characters.';
    } else {
        try {
            initial_setup_with_admin($_POST['admin_user'], $_POST['admin_pass']);
            $_SESSION[SETUP_SESSION] = SETUP_STATE_DONE;
        } catch (Exception $e) {
            $body->msg = $e->__toString();
        }
    }
}

switch ($_SESSION[SETUP_SESSION]) {
    case SETUP_STATE_FEATURE_CHECK:
        // TODO: feature check here and only advance state if it all passes
        $_SESSION[SETUP_SESSION] = SETUP_STATE_CREATE_ADMIN;
        $body->set_template('src/templates/setup_feature_check.php');
        break;

    case SETUP_STATE_CREATE_ADMIN:
        $body->set_template('src/templates/setup_create_admin.php');
        break;

    case SETUP_STATE_DONE:
        $_SESSION = array();
        session_destroy();
        $body->set_template('src/templates/setup_done.php');
        break;

}

echo $header->render();
echo $body->render();
echo $footer->render();